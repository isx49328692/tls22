#!/bin/bash

# Creamos directorio de certs
mkdir /etc/ldap/certs
# Copias y certificados
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/cacert.pem /etc/ldap/certs/.
#cp /opt/docker/cacert.pem /etc/ssl/certs/.
cp /opt/docker/servercert.ldap.pem /etc/ldap/certs/.
cp /opt/docker/serverkey.ldap.pem /etc/ldap/certs/.

# Configuracion previa
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
#slaptest -u -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

# Para que escuche tmb por ldapsy ldapi -d3
/usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"

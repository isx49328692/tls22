# Servidor LDAPs

Una vez configurados los files: \
Dockerfile \
startup.sh \
ldap.conf

	# TLS certificates (needed for GnuTLS) \
	#TLS_CACERT	/etc/ssl/certs/ca-certificates.crt \
	TLS_CACERT	/etc/ldap/certs/cacert.pem \
	TLS_KEY		/etc/ldap/certs/serverkey.ldap.pem \

	# Turning this off breaks GSSAPI used with krb5 when rdns = false \
	SASL_NOCANON	on \

slapd.conf

	include         /etc/ldap/schema/misc.schema \
	include		/etc/ldap/schema/core.schema \
	include		/etc/ldap/schema/cosine.schema \
	include		/etc/ldap/schema/inetorgperson.schema \
	include		/etc/ldap/schema/nis.schema \
	include		/etc/ldap/schema/openldap.schema \

	# Allow LDAPv2 client connections.  This is NOT the default. \
	allow bind_v2 \
	pidfile		/var/run/slapd/slapd.pid \
	TLSCACertificateFile        /etc/ldap/certs/cacert.pem \
	TLSCertificateFile          /etc/ldap/certs/servercert.ldap.pem \
	TLSCertificateKeyFile       /etc/ldap/certs/serverkey.ldap.pem \
	TLSVerifyClient       never \
	#TLSCipherSuite        HIGH:MEDIUM:LOW:+SSLv2 \

	openssl rand -hex 16 > cacert.srl

* La configuracion previa se mantiene


# Creamos los files (certificados, claves...)

## Geeramos calve privada --> cakey.pem

openssl genrsa -out cakey.pem

	Generating RSA private key, 2048 bit long modulus (2 primes)
	......................................+++++
	.......+++++
	e is 65537 (0x010001)

## Generamos certificado --> cacert.pem

openssl req -new -x509 -days 365 -key cakey.pem -out cacert.pem

	You are about to be asked to enter information that will be incorporated
	into your certificate request.
	What you are about to enter is what is called a Distinguished Name or a DN.
	There are quite a few fields but you can leave some blank
	For some fields there will be a default value,
	If you enter '.', the field will be left blank.
	-----
	Country Name (2 letter code) [AU]:CA
	State or Province Name (full name) [Some-State]:Barcelona
	Locality Name (eg, city) []:bcn
	Organization Name (eg, company) [Internet Widgits Pty Ltd]:VeritatAbsoluta
	Organizational Unit Name (eg, section) []:Certificats
	Common Name (e.g. server FQDN or YOUR name) []:veritat
	Email Address []:veritat@edt.org


## Generamos clave privada del servidor --> serverkey.ldap.pem
juan22@debian:~/Documents/m11/uf2/ldaps$ openssl genrsa -out serverkey.ldap.pem 4096

	Generating RSA private key, 4096 bit long modulus (2 primes)
	.................................................................++++
	...........................................................................................................++++
	e is 65537 (0x010001)




## Generamos req para el servidor --> servercert.ldap.pem

openssl req -new -x509 -days 365 -nodes -out servercert.ldap.pem -keyout serverkey.ldap.pem

	Generating a RSA private key
	..+++++
	.......+++++
	writing new private key to 'serverkey.ldap.pem'
	-----
	You are about to be asked to enter information that will be incorporated
	into your certificate request.
	What you are about to enter is what is called a Distinguished Name or a DN.
	There are quite a few fields but you can leave some blank
	For some fields there will be a default value,
	If you enter '.', the field will be left blank.
	-----
	Country Name (2 letter code) [AU]:CA
	State or Province Name (full name) [Some-State]:Barcelona
	Locality Name (eg, city) []:bcn
	Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
	Organizational Unit Name (eg, section) []:ldap
	Common Name (e.g. server FQDN or YOUR name) []:ldap.edt.org
	Email Address []:ldap@edt.org



## Generamos file request(peticion de firma a la CA VeritatAbsoluta) --> serverrequest.ldap.pem

openssl req -new -key serverkey.ldap.pem -out serverrequest.ldap.pem

	You are about to be asked to enter information that will be incorporated
	into your certificate request.
	What you are about to enter is what is called a Distinguished Name or a DN.
	There are quite a few fields but you can leave some blank
	For some fields there will be a default value,
	If you enter '.', the field will be left blank.
	-----
	Country Name (2 letter code) [AU]:CA
	State or Province Name (full name) [Some-State]:Barcelona
	Locality Name (eg, city) []:bcn
	Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
	Organizational Unit Name (eg, section) []:ldap
	Common Name (e.g. server FQDN or YOUR name) []:ldap.edt.org
	Email Address []:ldap@edt.org

	Please enter the following 'extra' attributes
	to be sent with your certificate request
	A challenge password []:jupiter
	An optional company name []:edt



## Firmamos el requst de ldap --> cacert.srl

openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in serverrequest.ldap.pem -CAcreateserial -days 365 -out servercert.ldap.pem 

	Signature ok
	subject=C = CA, ST = Barcelona, L = bcn, O = edt, OU = ldap, CN = ldap.edt.org, emailAddress = ldap@edt.org
	Getting CA Private Key






# Pruebas







